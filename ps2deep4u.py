#!/usr/bin/env python3

from io import BytesIO
from os import path
import sys
import argparse
import pycdlib

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

parser = argparse.ArgumentParser(description='PS2DEEP4U')
parser.add_argument('-i', '--iso', required=True, help='ISO file to open')
parser.add_argument('-l', '--list', action='store_true', 
    help='List content of ISO')
parser.add_argument('-p', '--print', help='Print file to standard output')
parser.add_argument('-e', '--extract', nargs=2,
    help='Extract file under directory')
parser.add_argument('-s', '--swap', nargs=2, help='Swap file content')
args = parser.parse_args()

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

iso = pycdlib.PyCdlib()
iso.open(args.iso)

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

def add_space(string, max_width):
  spaces = max_width - len(string)
  return ' ' * spaces
    
def walk_dirs(path):
  pwd = path

  for child in iso.list_children(iso_path=pwd):
    filename = child.file_identifier().decode('ASCII')
    fullpath = pwd + filename
    size = child.data_length
    lba = child.orig_extent_loc
    space1 = add_space(str(fullpath), 40) + '| '
    space2 = add_space(str(size), 12) + '| '

    print('{0}{1}{2}{3}{4}'.format(fullpath, space1, size, space2, lba))

    if child.is_dir() and not (filename == '.' or filename == '..'):
      walk_dirs(pwd + filename + '/')

def print_file(iso_path):
  extracted = BytesIO()
  iso.get_file_from_iso_fp(extracted, iso_path=iso_path)
  sys.stdout.buffer.write(extracted.getvalue())

def extract_file(iso_path, local_path):
  extracted = BytesIO()
  filename = iso_path.split('/')[-1]
  iso.get_file_from_iso_fp(extracted, iso_path=iso_path)
  with open(local_path + '/' + filename, 'wb') as local_file:
    local_file.write(extracted.getvalue())

def swap_file(iso_path, local_path):
  iso_file = iso.get_record(iso_path=iso_path)
  iso_file_size = iso_file.data_length
  iso_file_size_max = (int(iso_file_size / 2048) + 1 ) * 2048
  local_file_size = path.getsize(local_path) 
  if local_file_size <= iso_file_size_max:
    print('OK')
    with open(local_path, 'rb') as local_file:
      content = local_file.read()
      iso.modify_file_in_place(BytesIO(content), local_file_size, iso_path)
  else:
    print('ISO unmodified: Local file content too large for target ISO file.') 

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

if args.list:
  space1 = add_space(str('FILENAME'), 40) + '| '
  space2 = add_space(str('SIZE'), 12) + '| '
  print('FILENAME'+ space1 + 'SIZE' + space2 + 'LBA')
  print('-' * 70)

  walk_dirs('/')

if args.print:
  print_file(args.print)

if args.extract:
  extract_file(args.extract[0], args.extract[1])

if args.swap:
  swap_file(args.swap[0], args.swap[1])

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

iso.close()


